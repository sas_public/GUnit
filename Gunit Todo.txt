- Test status = enum pass, fail, skip, error
x Auto Run Test Method 
x Manual Run Test Method
- Setup
- Teardown
- Global Setup
- Global Teardown
x Pass if
- other assertions, equal, fail if, passiferror
x fix classes on test methods to be child classes.
- Error handling
- control not present in test method
- indicator not present in test method
x error in test method
- return error in test method as part of results
- avoid passif stomping on previous failures
- for CLI - add verbose flag. Add report type flags, help flag.
