# GUnit Unit Testing Framework for LabVIEW

*Inspired by JKI VITester*

# Purpose

This to be a Modernized Version of VI Tester.

# Desired improvements

- **Fewer Dependencies** - remove the OpenG dependencies
- **Handle New Datatypes** - sets, maps, and interfaces
- **Take advantages of new LabVIEW Features** - VIMs, PPLs, etc
- **Faster** - improve load time and test execution time
- **Mock Object Support** - make Mock Objects First Class Citizens
- **Inheritance Support** - if you have a test case for a parent class, you should be able to create a child of the test case to test a child class
- **Fix Global Setup Bug** - allow setting class data in global setup
- **Autorun Tests??** - investigate automatically running tests on save
- **Run all tests testing selected VI** - using QD quickly run all tests that could potentially call a specific VI - makes feedback even quicker.
- **Run individual Test** - using QD quickly run setup/teardown and specific test for quick debugging

# Contributors

- Sam Taggart
- Christian Butcher
